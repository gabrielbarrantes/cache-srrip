#ifndef CACHEBLOCK_H_
#define CACHEBLOCK_H_

//Struct for representing a cache block

typedef struct 
{
    int m_iValidBit;
    int m_iDirtyBit;
    unsigned int m_uiTag;
    int m_iRRPV;
} cacheBlock ;

#endif /* CACHEBLOCK_H_ */
