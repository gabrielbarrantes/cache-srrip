# Cache Simulator-SRRIP

A simulator of a cache with SRRIP replacement policy, determining the miss rate 
and other statistics of interest.

The program takes a input file with the list of memory addresses, and simulates 
the behavior of the cache keeping track of the misses and hits.  

## Example of input file:

        LS Address  IC

        # 0 7fffed80 2
        # 0 10010000 5
        # 0 10010060 3
        # 0 10010030 10
        # 0 10010004 4
        # 0 10010064 6
        # 0 10010034 7

## Mode of use:

$ gunzip -c mcf.trace.gz | cache -t < # > -a < # > -l < # >

Where -t is the cache size in KB, -a is the associativity and -l the line length.

### Example of use:
    $ gunzip -c mcf.trace.gz | ./cache -t 32 -a 8 -l 64

### Output:
        Cache parameters:
        Cache Size (KB)			32
        Cache Associativity		        8
        Cache Block (line) Size (bytes)	64
        
        Simulation results:
        Overall miss rate 0.302169
        Read miss rate    0.253740
        Dirty evictions   988780
        Load  misses      1418271
        Store misses      679945
        Total misses      2098216
        Load  hits        4171201
        Store hits        674440
        Total hits        4845641
