/*
 * CalculateDeviation.cpp
 *
 *  Created on: Set 21, 2018
 *      Author: gab
 */

#include <math.h>
#include "cacheSRRIP.h"

#define wordSizeBytes 1
#define addressSizeBits  32

CacheSRRIP::CacheSRRIP(int i_iSizeKB, int i_iBlockSizeBytes, int i_iAssociativity)
{
    m_iSize = i_iSizeKB;
    m_iAssociativity = i_iAssociativity;
    m_iBlockSizeBytes = i_iBlockSizeBytes;
    
    if(i_iAssociativity>2) { m_iRereferenceIntervalLength=4;}
    if(i_iAssociativity<=2){ m_iRereferenceIntervalLength=2;}
    int l_iCacheSizeBytes = (int) pow (2, (int) round( log((double) (i_iSizeKB*1000))/log(2) ));
    
    m_iNumberOfSets = l_iCacheSizeBytes/(m_iBlockSizeBytes*m_iAssociativity);
    m_iNumberOfVias = i_iAssociativity;
    
    m_iNumberOfIndexBits  = (int) (log(m_iNumberOfSets)/log(2));
    m_iNumberOfOffsetBits = (int) (log(i_iBlockSizeBytes/wordSizeBytes)/log(2));
    m_iNumberOfTagBits    = addressSizeBits - m_iNumberOfIndexBits - m_iNumberOfOffsetBits;
    
    //Generate masks
    m_uiIndexMask = 0;
    for(int i=0; i<m_iNumberOfIndexBits;i++){m_uiIndexMask = (m_uiIndexMask<<1)+1;}
    
    m_uiTagMask = 0;
    for(int i=0; i<m_iNumberOfTagBits;i++){m_uiTagMask = (m_uiTagMask<<1)+1;}

    //Set initial values
    m_dOverallMissrate  = 0.0;
    m_dReadMissRate     = 0.0;
    m_uiDirtyEvictions  = 0;
    m_uiLoadMisses      = 0;
    m_uiStoreMisses     = 0;
    m_uiTotalMisses     = 0;
    m_uiLoadHits        = 0;
    m_uiStoreHits       = 0;
    m_uiTotalHits       = 0;
    m_uiTotalHits       = 0;
    
    //Allocate memory for cache's blocks
    m_pCache = (cacheBlock **)malloc (m_iNumberOfSets*sizeof(cacheBlock *));
    //if (cache  == NULL){ return 1; }
    for (int i=0;i<m_iNumberOfSets;i++){m_pCache[i] = (cacheBlock *) malloc (m_iNumberOfVias*sizeof(cacheBlock));}
    //Set the initial values of cache
    for(int i=0;i<m_iNumberOfSets;i++)
    {
        for(int j=0;j<m_iNumberOfVias;j++)
        {
            m_pCache[i][j].m_iValidBit = 0; 
            m_pCache[i][j].m_iDirtyBit = 0;
            m_pCache[i][j].m_iRRPV = m_iRereferenceIntervalLength-1;
        }
    }
    
}

void CacheSRRIP::DataAccess(long i_liAddress, int i_iLoadStore)
{
    unsigned int index = (i_liAddress>> m_iNumberOfOffsetBits) & m_uiIndexMask;
    unsigned int tag   = (i_liAddress>> (m_iNumberOfIndexBits+m_iNumberOfOffsetBits) ) & m_uiTagMask;
    
    int hit =0;
    int field = -1;
    //Look for Hit
    for(int j=0; j< m_iNumberOfVias; j++)
    {
        if((m_pCache[index][j].m_uiTag == tag) && (m_pCache[index][j].m_iValidBit==1))
        { 
            hit = 1;            
            m_pCache[index][j].m_iRRPV=0;
            if(i_iLoadStore){ m_uiStoreHits++; m_pCache[index][j].m_iDirtyBit =1; }
            else { m_uiLoadHits++; }
            
            break;
        }
    }
    //if not hit, look envit candidate and replace the block in cache
    if(hit==0)
    {
        int envitCandidate=-1;
        while(envitCandidate==-1)
        {
            for(int j=0; j< m_iNumberOfVias; j++)
            {
                if(m_pCache[index][j].m_iRRPV >= m_iRereferenceIntervalLength-1 || m_pCache[index][j].m_iValidBit ==0){ envitCandidate=j; break;}
            }
            if(envitCandidate==-1)
            {  
                for(int j=0; j< m_iNumberOfVias; j++)
                {
                    m_pCache[index][j].m_iRRPV = m_pCache[index][j].m_iRRPV+1;
                }
            }
        }
        
        if(m_pCache[index][envitCandidate].m_iValidBit==1 && m_pCache[index][envitCandidate].m_iDirtyBit==1){m_uiDirtyEvictions++;}
        
        m_pCache[index][envitCandidate].m_uiTag = tag;
        m_pCache[index][envitCandidate].m_iRRPV = m_iRereferenceIntervalLength-2;
        m_pCache[index][envitCandidate].m_iValidBit = 1;
        if(i_iLoadStore)
        {
            m_uiStoreMisses = m_uiStoreMisses+1; 
            m_pCache[index][envitCandidate].m_iDirtyBit =1;
        } else {
            m_uiLoadMisses ++;
            m_pCache[index][envitCandidate].m_iDirtyBit = 0; 
        }
    }
}

void CacheSRRIP::CalculateStatistics()
{
    m_uiTotalMisses=m_uiStoreMisses+m_uiLoadMisses;
    m_uiTotalHits=m_uiStoreHits+m_uiLoadHits;
    m_dReadMissRate= ((double ) (m_uiLoadMisses) )/ ((double) (m_uiLoadMisses+m_uiLoadHits) );
    m_dOverallMissrate= ((double) (m_uiTotalMisses) )/((double) (m_uiTotalMisses+m_uiTotalHits));
}
