#include <stdio.h>
#include <stdlib.h>
#include "cacheSRRIP.h"
#include <string.h>
#include <netinet/in.h>

int g_iAssociativity;         // Associativity of cache
int g_iBlocksize_bytes;       // Cache Block size in bytes
int g_iCachesize_kb;          // Cache size in KB

//Prints the usage of the program
void print_usage ()
{
    printf ("Usage: gunzip -c <tracefile> | ./cache -t size -a assoc -l ll \n");
    printf ("  tracefile : The memory trace file\n");
    printf ("  -t size : The size (in KB) of the cache\n");
    printf ("  -a assoc : The associativity of the cache\n");
    printf ("  -l ll : The line length (in bytes) of the cache\n");
    exit (0);
}

int main(int argc, char * argv []) {
    // Process the command line arguments
    // Process the command line arguments
    int j = 1;
    if (argc <=1){print_usage ();}
    while (j < argc) 
    {
        if (strcmp ("-t", argv [j]) == 0) 
        {
          j++;
          if (j >= argc)
            print_usage ();
          g_iCachesize_kb = atoi (argv [j]);
          j++;
        } else if (strcmp ("-a", argv [j]) == 0) {
          j++;
          if (j >= argc)
            print_usage ();
          g_iAssociativity = atoi (argv [j]);
          j++;
        } else if (strcmp ("-l", argv [j]) == 0) {
          j++;
          if (j >= argc)
            print_usage ();
          g_iBlocksize_bytes = atoi (argv [j]);
          j++;
        } else {
          print_usage ();
        }
    }
    //Create the cache
    CacheSRRIP cache(g_iCachesize_kb, g_iBlocksize_bytes, g_iAssociativity);

    // Variables for store the read address and load/Store operation//
    int  l_iLoadStore;
    int  l_iICount;
    char l_cMarker;
    long l_lAddress;

    //Process address by address
    while (scanf("%c %d %lx %d\n",&l_cMarker,&l_iLoadStore,&l_lAddress,&l_iICount) != EOF) 
    {        
        cache.DataAccess(l_lAddress, l_iLoadStore);//do the data access in the cache
    }
    ////////////////////////////////////////////////////////////////////
    //                                                                //
    //                        Print Results                           //
    //                                                                //
    ////////////////////////////////////////////////////////////////////

    //Print out cache configuration
    printf("Cache parameters:\n");
    printf("Cache Size (KB)\t\t\t%d\n", cache.m_iSize);
    printf("Cache Associativity\t\t%d\n", cache.m_iAssociativity);
    printf("Cache Block (line) Size (bytes)\t%d\n", cache.m_iBlockSizeBytes);
    printf("\n");
    
    //Print out results
    cache.CalculateStatistics(); //calculate results
    printf("Simulation results:\n");

    printf("Overall miss rate %lf\n", cache.m_dOverallMissrate);
    printf("Read miss rate    %lf\n", cache.m_dReadMissRate);
    printf("Dirty evictions   %u\n" , cache.m_uiDirtyEvictions);
    printf("Load  misses      %u\n" , cache.m_uiLoadMisses);
    printf("Store misses      %u\n" , cache.m_uiStoreMisses);
    printf("Total misses      %u\n" , cache.m_uiTotalMisses);
    printf("Load  hits        %u\n" , cache.m_uiLoadHits);
    printf("Store hits        %u\n" , cache.m_uiStoreHits);
    printf("Total hits        %u\n" , cache.m_uiTotalHits);

    return 0;
}
