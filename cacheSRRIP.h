/*
 * CalculateDeviation.hpp
 *
 *  Created on: Set 21, 2018
 *      Author: gab
 */

#ifndef CACHESRRIP_HPP_
#define CACHESRRIP_HPP_

#include "cacheBlock.h"

class CacheSRRIP
{
    public:
        CacheSRRIP(int sizeKB, int blockSizeBytes, int associativity);
        
        void DataAccess(long i_liAddress, int i_iLoadStore);
        void CalculateStatistics();
        
        int m_iSize;
        int m_iAssociativity;
        int m_iBlockSizeBytes;
        
        double       m_dOverallMissrate;
        double       m_dReadMissRate;
        unsigned int m_uiDirtyEvictions;
        unsigned int m_uiLoadMisses;
        unsigned int m_uiStoreMisses;
        unsigned int m_uiTotalMisses;
        unsigned int m_uiLoadHits;
        unsigned int m_uiStoreHits;
        unsigned int m_uiTotalHits;
        
    protected:
    private:
        int m_iNumberOfOffsetBits;
        int m_iNumberOfIndexBits;
        int m_iNumberOfTagBits;
        int m_iRereferenceIntervalLength;
        int m_iNumberOfSets;
        int m_iNumberOfVias;
        unsigned int m_uiIndexMask;
        unsigned int m_uiTagMask;
        cacheBlock **m_pCache;
};

#endif /* CACHESRRIP_HPP_ */
